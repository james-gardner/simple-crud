# Cakes (Simple CRUD example)

## Running Offline

First make sure you've got everything installed:

```
yarn install
sls dynamodb install --stage local
```

To run the API locally open up a terminal (Linux, MacOS and WSL) and run:

1) Start DynamoDB locally:

```
sls dynamodb start --stage local -m --seed
```

This will load some example data (cakes.json) into your offline database.

2) Run the API:

```
ENVIRONMENT=local sls offline --stage local --aws-profile={YOUR_PROFILE} --region=eu-west-1
```

Note: --aws-profile is optional if you're happy to use default.

3) Run the App:

```
yarn serve:web
```

This project makes use of dotenv for configuration. You'll need to have a .env file set up containing the following for development:

```
VITE_API_URL=http://localhost:8123
```
