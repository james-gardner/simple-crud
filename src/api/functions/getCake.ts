import AWS from 'aws-sdk';

import * as Types from '../../types';
import { CakeModel } from '../models';

export const getCake = async (id: string, client: AWS.DynamoDB.DocumentClient): Promise<Types.Cake | undefined> => {
  console.log(CakeModel.key(id));
  const result = await client.get({
    TableName: process.env.DYNAMODB_TABLE_NAME!,
    Key: CakeModel.key(id)
  }).promise();

  if (!result.Item) return;

  const {
   name,
   comment,
   imageUrl,
   yumFactor
  } = result.Item;

  return {
    id,
    name,
    comment,
    imageUrl,
    yumFactor
  };
};