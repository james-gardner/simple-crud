import * as Types from '../../types';

export const scan = async (params: AWS.DynamoDB.DocumentClient.ScanInput, client: AWS.DynamoDB.DocumentClient) => {
  const results = [];
  let items;

  do {
    items = await client.scan(params).promise();

    if (!items.Items) {
      return results;
    }

    for (const item of items.Items) {
      results.push(item);
    }

    params.ExclusiveStartKey = items.LastEvaluatedKey;
  } while (items.LastEvaluatedKey);

  return results;
};

export const getCakes = async (client: AWS.DynamoDB.DocumentClient): Promise<Types.Cake[] | []> => {
  const results = await scan({
    TableName: process.env.DYNAMODB_TABLE_NAME!,
  }, client);

  console.log(results);
  return results as Types.Cake[];
};