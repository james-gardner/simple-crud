import * as Types from '../../types';

export class CakeModel {
  cake: Types.Cake;

  static key(id: string) {
    return {
      PK: `CAKE#${id}`,
    };
  }

  constructor(cake: Types.Cake ) {
    this.cake = cake;
  }

  toItem() {
    const { id, ...cake } = this.cake;

    return {
      ...CakeModel.key(id),
      ...cake
    };
  }
}

export const createCake = (
  cake: Types.Cake
) => new CakeModel(cake);
