import AWS from 'aws-sdk';

import * as Types from '../types';
import * as Functions from './functions';

const config = (process.env.ENVIRONMENT === 'local') ? {
  region: 'localhost',
  endpoint: 'http://localhost:8125' // TODO: Use .env.
} : undefined;

const dynamodb = new AWS.DynamoDB.DocumentClient(config);

/**
 * Helper for generic success response.
 * Basic CORs headers example. Use middy or something in a real project.
 */
const success = <T>(body?: T | undefined, statusCode = 200) => {
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    ...(body && {
      body: JSON.stringify(body)
    })
  };
};

export const handler = async (event: AWSLambda.APIGatewayProxyEvent) => {
  const id = event.pathParameters?.id;

  try {
    switch(event.httpMethod) {
      case 'GET':
        if (id) {
          const cake = await Functions.getCake(id, dynamodb);
          if (cake) {
            return success<Types.Cake>(cake);
          }

          return { statusCode: 404 };
        }

        return success<Types.Cake[]>((await Functions.getCakes(dynamodb)));

      default:
        return { statusCode: 405 };
    }
  } catch (err) {
    // TODO: Catch DB related errors;
    return { statusCode: 500, body: err.message};
  }
};
