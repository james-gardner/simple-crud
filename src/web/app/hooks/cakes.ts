import axios from 'axios';
import { useMutation,useQuery } from 'react-query';

import * as Types from '../../../types';


axios.defaults.baseURL = import.meta.env.VITE_API_URL;

export const useCakes = () =>
  useQuery('cakes', () =>
    axios.get(`/v1/cakes`).then(res => res.data)
  );

export const useCake = (id: string) =>
  useQuery(['cakes', id], () =>
    axios.get(`/v1/cakes/${id}`).then(res => res.data)
  );

export const useCreateCake = () =>
  useMutation((values: Omit<Types.Cake, 'PK' | 'id'>) =>
    axios.post('/v1/cakes', values)
  );
