import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import { FormProvider,useForm } from 'react-hook-form';

import {
  useCreateCake
} from '../hooks/cakes';
import { CakeForm } from './CakeForm';


interface CakeDialogProps {
  open: boolean;
  onClose: (value: string) => void;
}

export const CakeDialog = ({
  open,
  onClose
}: CakeDialogProps) => {
  const { mutate } = useCreateCake();
  const methods = useForm();

  const handleClose = () => {
    mutate({
      name: 'test',
      comment: 'meh',
      yumFactor: 4,
      imageUrl: ''
    });

    onClose('meh');
  };

  const handleSubmit = () => {
    console.log(methods.getValues());
    mutate({
      name: 'test',
      comment: 'meh',
      yumFactor: 4,
      imageUrl: ''
    });
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      disableBackdropClick
    >
      <DialogTitle id="simple-dialog-title">Create a new Cake</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To create a new cake complete the form below.
          </DialogContentText>
          <FormProvider {...methods}>
            <CakeForm />
          </FormProvider>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Save
          </Button>
        </DialogActions>
    </Dialog>
  );
};