import Slider from '@material-ui/core/Slider';
import { Mark } from '@material-ui/core/Slider/Slider';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  root: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(4)
  }
}));

interface YumFactorProps {
  max: number;
  min: number;
};

export const YumFactor = ({
  max,
  min
}: YumFactorProps) => {
  const classes = useStyles();

  const marks :Mark[] = [...Array(max + 1).keys()].map(value => ({
    value,
    label: value
  }));

  return (
    <div className={classes.root}>
      <Typography id="yumFactor" gutterBottom color="textSecondary">
        Yum Factor
      </Typography>
      <Slider
        defaultValue={1}
        aria-labelledby="yumFactor"
        step={1}
        marks={marks}
        min={min}
        max={max}
        valueLabelDisplay="auto"
      />
    </div>
  );
};