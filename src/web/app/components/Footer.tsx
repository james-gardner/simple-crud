import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { makeStyles } from '@material-ui/core/styles';
import PlusIcon from '@material-ui/icons/Add';
import React from 'react';

interface FooterProps {
  onChange: (event: React.ChangeEvent<any>, newValue: string) => void;
}

const useStyles = makeStyles({
  root: {
    'padding-bottom': '45px'
  },
  sticky: {
    position: 'fixed',
    width: '100%',
    bottom: 0
  }
});

export const Footer = ({
  onChange
}: FooterProps) => {
  const classes = useStyles();

  return (
    <BottomNavigation
      className={classes.sticky}
      showLabels
      onChange={onChange}
    >
      <BottomNavigationAction label="Add a New Cake" value="new" icon={<PlusIcon />} />
    </BottomNavigation>
  );
};