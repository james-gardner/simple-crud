import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import { Controller,useFormContext } from 'react-hook-form';

import { YumFactor } from './YumFactor';

const useStyles = makeStyles((theme) => ({
  root: {
    'max-width': '100%',
  },
  input: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(2),
  }
}));

export const CakeForm = () => {
  const classes = useStyles();

  const {
    control
  } = useFormContext();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <Controller
        name="name"
        control={control}
        defaultValue=""
        render={({ field: { onChange, value } }) => (
          <TextField
            className={classes.input}
            label="Name"
            fullWidth
            value={value}
            onChange={onChange}
          />
        )}
      />
      <Controller
        name="comment"
        control={control}
        defaultValue=""
        render={({ field: { onChange, value } }) => (
          <TextField
            className={classes.input}
            label="Comment"
            fullWidth
            multiline
            value={value}
            onChange={onChange}
          />
        )}
      />

      <YumFactor min={1} max={5}/>
    </form>
  );
};
