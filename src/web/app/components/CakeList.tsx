import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';

import * as Types from '../../../types';

interface CakeListProps {
  cakes: Types.Cake[];
};

const useStyles = makeStyles(() => ({
  root: {
    width: '100%'
  },
}));

export const CakeList = ({
  cakes
}: CakeListProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main cakes">
        { cakes && cakes.map(cake => (
          <ListItem button key={`cake_${cake.id}`}>
            <ListItemText primary={cake.name} />
            <ListItemAvatar>
              <Avatar alt={cake.name} src="/static/images/avatar/1.jpg" />
            </ListItemAvatar>
          </ListItem>
        ))}
      </List>
    </div>
  );
};
