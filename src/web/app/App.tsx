import { makeStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';

import {
  CakeDialog,
  CakeList,
  Footer
} from './components';
import {
  useCakes,
} from './hooks/cakes';

const useStyles = makeStyles({
  app: {
    'padding-bottom': '45px'
  },
  h2: {
    color: 'pink'
  }
});

const App = () => {
  const {
    isSuccess,
    data
  } = useCakes();

  const classes = useStyles();

  const [ showCakeDialog, setShowCakeDialog ] = useState(false);


  const handleDialogOpen = () =>
    setShowCakeDialog(true);

  const handleDialogClose = () =>
    setShowCakeDialog(false);

  return (
    <div className={classes.app}>
      <h2>Cakes O'Plenty</h2>
      { isSuccess && data?.length && (
        <>
          <CakeList cakes={data} />
          <CakeDialog
            open={showCakeDialog}
            onClose={handleDialogClose}
          />
          <Footer
            onChange={handleDialogOpen}
          />
        </>
      )}
    </div>
  );
};

export default App;
