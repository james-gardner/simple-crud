module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['./tsconfig.json'],
    sourceType: 'module',
    tsconfigRootDir: __dirname,
  },
  env: {
    node: true,
  },
  ignorePatterns: ['dist', '.*.js', '*.config.js', 'node_modules'],
  plugins: ['@typescript-eslint', 'node', 'simple-import-sort'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:node/recommended',
    'prettier',
    'prettier/@typescript-eslint',
  ],
  rules: {
    'node/no-unpublished-import': 'off',
    semi: ['error'],
    eqeqeq: ['error', 'always'],
    'sort-imports': 'off',
    'import/order': 'off',
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',
    'node/no-unsupported-features/es-syntax': 'off',
    'no-unused-vars': ['off'],
    // Turn back on when ready.
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    // ...
    '@typescript-eslint/no-unused-vars': ['warn', { argsIgnorePattern: '^_' }],
    'node/no-missing-import': [
      'error',
      {
        tryExtensions: ['.js', '.ts', '.tsx', '.json'],
      },
    ],
  },
};
